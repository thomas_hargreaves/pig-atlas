"""
This is the base class that contains methods that are used through out the helper classes
"""


class Base(object):
    def __init__(self):
        self._context = None
        self._sceneviewer = None

    def setSceneviewer(self, sceneviewer):
        self._sceneviewer = sceneviewer
        # if sceneviewer.getTool() is not None:
        #     self._tool = sceneviewer.getTool()

    def getSceneviewer(self):
        return self._sceneviewer

    def setContext(self, context):
        self._context = context