"""
Contains the code that changes the muscles
"""

import random

from PySide import QtGui, QtCore
from opencmiss.zinc.field import Field, FieldGroup
from opencmiss.zinc.material import Material

from base_class import Base
from zinc.zinc_helper import get_fieldmodule, get_scene, get_materialmodule


class Musculature(QtGui.QWidget, Base):

    muscle_name = QtCore.Signal(FieldGroup)
    check_edit_state = QtCore.Signal(bool)

    def __init__(self):

        super(Musculature, self).__init__()
        self._muscleList = None
        self._colour_group = None
        self._visibility_group = None

        self._muscleNames = []

    def setListWidget(self, muscleList):
        """
        This sets up the tablewidget with the click events and the spans.
        """
        self._muscleList = muscleList

        # For testing purposes add a muscle
        self._sceneviewer.graphicsInitialized.connect(self.resizeColumns)

        # Merge and make the final row unselectable but have a double click ability.
        # double_row = self._muscleList.item(0, 0)
        # double_row.setFlags(QtCore.Qt.NoItemFlags | QtCore.Qt.ItemIsEnabled)
        # self._muscleList.setSpan(0, 0, 1, 3)

        # self._muscleList.currentCellChanged.connect(self.stopSelection)
        # self._muscleList.cellDoubleClicked.connect(self.addMuscleRow)
        self._muscleList.itemSelectionChanged.connect(self.updateZinc)

    def setButtonGroups(self, colour, visibility=None):

        self._colour_group = colour
        self._colour_group.buttonClicked.connect(self.showColourPicker)
        self._visibility_group = visibility
        self._visibility_group.buttonClicked.connect(self.toggleVisibility)

    @QtCore.Slot()
    def resizeColumns(self):
        """
        Slot called after the graphics have been initialised so the columns look good.
        """
        width = self._muscleList.width()

        self._muscleList.setColumnWidth(0, width - 129)
        self._muscleList.setColumnWidth(1, 55)
        self._muscleList.setColumnWidth(2, 55)

    @QtCore.Slot(QtGui.QAbstractButton)
    def processMuscleButton(self, button):

        name = button.objectName()

        if name == 'muscle_rename':
           self.renameMuscle()
        elif name == 'muscle_remove':
            self.removeMuscle()
        else:
            raise AttributeError("Unknown Object in Button Group")

    def renameMuscle(self):
        """
        Renames the muscle
        """
        row = self._muscleList.currentRow()
        if row == self._muscleList.rowCount() - 1:
            return

        widget = self._muscleList.cellWidget(row, 0)
        old_name = str(widget.text())

        while True:
            new_name, status = QtGui.QInputDialog.getText(self, "Please enter the muscle name", "Muscle Name")
            new_name = str(new_name)

            if new_name is None:
                QtGui.QMessageBox.warning(self, "Error", "Muscle Names can't be blank")
                continue
            if new_name in self._muscleNames:
                QtGui.QMessageBox.warning(self, "Error", "Please enter a different name")
                continue
            if new_name == old_name:
                QtGui.QMessageBox.warning(self, "Error", "Please enter a different name")
                continue
            if status == QtGui.QDialog.Accepted:
                self._muscleNames.remove(old_name)
                self._muscleNames.append(new_name)
                break
            else:
                return

        row = self._muscleList.currentRow()
        if row is not None:

            widget.setText(new_name)
            region = self._context.getDefaultRegion()

            with get_fieldmodule(region) as fieldmodule:
                group = fieldmodule.findFieldByName(old_name)
                if group.isValid():
                    group.setName(new_name)

            with get_scene(region) as scene:
                graphics = scene.findGraphicsByName(old_name)
                if graphics.isValid():
                    graphics.setName(new_name)

    @QtCore.Slot(QtGui.QAbstractButton)
    def toggleVisibility(self, button):
        """
        This slot is called when the tickbox is clicked.
        It checks what button is pressed and then hids the associated graphics.
        """
        for r in range(self._muscleList.rowCount()):
            if self._muscleList.cellWidget(r, 2) == button:
                row = r
                break
        else:
            raise AttributeError("No button clicked")

        name = self._muscleList.cellWidget(row, 0).text()
        name = str(name)

        # with get_scene(self._context.getDefaultRegion()) as scene:
        #     surface = scene.findGraphicsByName(name)
        #     if surface.isValid():
        #         state = button.isChecked()
        #         surface.setVisibilityFlag(state)

        state = button.isChecked()
        region = self._context.getDefaultRegion().findSubregionAtPath('Muscle/{}'.format(name))
        with get_scene(region) as scene:
            scene.setVisibilityFlag(state)

    def removeMuscle(self):
        """
        This function removes the Elements, Nodes, Field Group, graphics and row from the table.
        """
        row = self._muscleList.currentRow()
        if row == self._muscleList.rowCount() - 1:
            return

        name = str(self._muscleList.cellWidget(row, 0).text())
        self._muscleNames.remove(name)

        region = self._context.getDefaultRegion()
        with get_fieldmodule(region) as fieldmodule:
            group = fieldmodule.findFieldByName(name).castGroup()
            if group.isValid():
                if group.isEmpty():
                    group.clear()
                    self._muscleList.removeRow(row)
                else:
                    nodest = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                    mesh = fieldmodule.findMeshByDimension(2)

                    elementGroup = group.getFieldElementGroup(mesh)
                    if elementGroup.isValid():
                        elementGroup.getMeshGroup().destroyAllElements()

                    nodeGroup = group.getFieldNodeGroup(nodest)
                    if nodeGroup.isValid():
                        nodeGroup.getNodesetGroup().destroyAllNodes()

                    group.clear()
                    self._muscleList.removeRow(row)

            group.setManaged(False)

        with get_scene(region) as scene:
            graphics = scene.findGraphicsByName(name)
            if graphics.isValid():
                scene.removeGraphics(graphics)
            else:
                print "Graphics already removed"

        if self._muscleList.rowCount() < 2:
            self.check_edit_state.emit(False)
        # else:
        #     self.check_edit_state.emit(True)

    @QtCore.Slot(QtGui.QAbstractButton)
    def showColourPicker(self, button):
        """
        This Slot is called when the colour button is clicked.
        The button colour is then changed along with the associated graphics.
        """
        colour = QtGui.QColorDialog.getColor(initial=QtCore.Qt.darkBlue)
        stylesheet = "QPushButton {{ background-color : {};}}".format(colour.name())
        button.setStyleSheet(stylesheet)

        for r in range(self._muscleList.rowCount()):
            if self._muscleList.cellWidget(r, 1) == button:
                row = r
                break
        else:
            raise AttributeError("No button clicked")

        name = str(self._muscleList.cellWidget(row, 0).text())

        with get_materialmodule(self._context) as materialmodule:
            material = materialmodule.findMaterialByName(name)
            rgb = list(colour.getRgbF())
            if material.isValid():
                material.setAttributeReal3(Material.ATTRIBUTE_DIFFUSE, rgb[:3])
                material.setAttributeReal(Material.ATTRIBUTE_ALPHA, rgb[3])

    @QtCore.Slot(int, int)
    def addMuscleRow(self, row, column, name):
        """
        This Slot is called when any cell is double clicked. It only triggers when the last row is double clicked.
        A dialog pops asking for the name input.
        It checks if the name is already on the muscle names list or if empty.
        It adds it to the muscle names list and then creates a field group and graphics.
        """
        if row is None or column is None or column != 0 or row < self._muscleList.rowCount() - 1:
            return

        # while True:
        #     # name, status = QtGui.QInputDialog.getText(self, "Please enter the muscle name", "Muscle Name")
        #     # name = str(name)
        #
        #     if name is None:
        #         QtGui.QMessageBox.warning(self, "Error", "Muscle Names can't be blank")
        #         continue
        #     if name in self._muscleNames:
        #         QtGui.QMessageBox.warning(self, "Error", "Please enter a different name")
        #         continue
        #     if status == QtGui.QDialog.Accepted:
        #         self._muscleNames.append(name)
        #         break
        #     else:
        #         return

        self._muscleNames.append(name)

        row_count = self._muscleList.rowCount()
        self._muscleList.insertRow(row_count)

        # Name
        label = QtGui.QLabel()
        label.setText(name)
        self._muscleList.setCellWidget(row_count, 0, label)

        # Colour
        colour = QtGui.QPushButton()
        colour.setFixedWidth(40)
        colour.setFixedHeight(30)
        self._colour_group.addButton(colour)
        self._muscleList.setCellWidget(row_count, 1, colour)

        # Click Button if visible
        tick = QtGui.QCheckBox()
        tick.setChecked(True)
        self._muscleList.setCellWidget(row_count, 2, tick)
        self._visibility_group.addButton(tick)

        # Using zinc it creates a new graphic with the new group
        region = self._context.getDefaultRegion()
        self.setMuscleGroup(name)

        # Add a random colour
        random_colour = QtGui.QColor()
        rgb = [random.random(), random.random(), random.random()]
        random_colour.setRgbF(*rgb)
        Stylesheet = "QPushButton {{ background-color : {};}}".format(random_colour.name())
        colour.setStyleSheet(Stylesheet)

        self._muscleList.setCurrentCell(row, 0)

        return rgb

    @QtCore.Slot(int, int, int, int)
    def stopSelection(self, currentRow, currentColumn, previousRow, previousColumn):
        """
        Stops the Double Click row becoming selected.
        """
        row_count = self._muscleList.rowCount()
        if row_count > 1 and currentRow == row_count - 1:
            self._muscleList.selectRow(previousRow)

    @QtCore.Slot()
    def updateZinc(self):
        """
        This function is called every time the current muscle selection is changed.
        """

        row = self._muscleList.currentRow()

        if row == self._muscleList.rowCount() - 1:
            return

        name = self._muscleList.cellWidget(row, 0).text()

        # Seems to make sure the row is selected.
        self._muscleList.selectRow(row)
        self.setMuscleGroup(name)

    def setMuscleGroup(self, name):
        """
        This method creates a FieldGroup and the two associated subgroups.
        """
        name = str(name)
        with get_fieldmodule(self._context.getDefaultRegion()) as fieldmodule:
            group = fieldmodule.findFieldByName(name)
            if group.isValid():
                group = group.castGroup()
            else:
                group = fieldmodule.createFieldGroup()
                group.setManaged(True)
                nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                group.createFieldNodeGroup(nodeset)
                mesh = fieldmodule.findMeshByDimension(2)
                group.setName(name)
                group.createFieldElementGroup(mesh)

        self.muscle_name.emit(group)

    @QtCore.Slot(bool)
    def toggleSkin(self, state):
        region = self._context.getDefaultRegion().findChildByName('Skin')
        with get_scene(region) as scene:
            scene.setVisibilityFlag(state)

    @QtCore.Slot(bool)
    def toggleSkeleton(self, state):
        region = self._context.getDefaultRegion()
        with get_scene(region) as scene:
            graphics = scene.findGraphicsByName('skeleton')
            graphics.setVisibilityFlag(state)

    @QtCore.Slot(bool)
    def toggleOrgans(self, state):
        region = self._context.getDefaultRegion().findChildByName('Organ')
        with get_scene(region) as scene:
            scene.setVisibilityFlag(state)

    def hideAllMuscles(self, state):
        region = self._context.getDefaultRegion().findChildByName('Muscle')
        with get_scene(region) as scene:
            scene.setVisibilityFlag(not state)