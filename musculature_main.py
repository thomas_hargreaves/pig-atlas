import ctypes
import sys
from os import listdir
from os.path import isfile, join

from PySide import QtGui
from opencmiss.zinc.context import Context

from viewer_widget import ViewerWidget

"""
This is the main program. It creates the context and displays the GUI. It isn't a class. Not sure if this is a problem
Creates the context, GUI. It checks to make sure everything isn't None and then runs the GUI.
"""

context = Context('Musculature creator')
context.getMaterialmodule().defineStandardMaterials()
context.getGlyphmodule().defineStandardGlyphs()

app = QtGui.QApplication(sys.argv)

# filenames, selected_filter = QtGui.QFileDialog.getOpenFileNames(None, "Please select the skeleton files",
#                                                                 "",
#                                                                 "exfiles (*.exnode *.exelem)")


mypath = 'Pig Files/Skeleton'
filenames = [join(mypath, f) for f in listdir(mypath) if isfile(join(mypath, f))]

app.setWindowIcon(QtGui.QIcon('logo.ico'))
myappid = u'Pig Atlas v1.0'
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

e = ViewerWidget(context, filenames)
e.show()
sys.exit(app.exec_())






