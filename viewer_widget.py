from os import listdir
from os.path import isfile, join, split, splitext

from PySide import QtCore, QtGui
from opencmiss.zinc.field import Field
from opencmiss.zinc.graphics import Graphics
from opencmiss.zinc.material import Material

from UI.viewer_ui import Ui_Widget
from musculature import Musculature
from zinc.zinc_helper import *


class ViewerWidget(QtGui.QWidget):
    """
    This class is a the viewer window. It takes the UI from another file. It stores the location of the sceneviewer
    widget so that it can be passed up to the main program if required.
    Helper classes will act on this sceneviewer.
    Any changes that need to be fixed from the generated file will be applied in this
    """

    def __init__(self, context, filenames):
        """
        Sets the context and the UI for the GUI up.
        :param context: The OpenCMISS zinc context that is passed down to the widgets that need it.
        :return: None
        """
        super(ViewerWidget, self).__init__()

        self._ui = None
        self._ui = Ui_Widget()
        self._ui.setupUi(self)
        self._context = context
        self._sceneviewer = self._ui.sceneviewerWidget
        self._sceneviewer.setContext(context)

        self._filenames = filenames
        # self._tool = None

        # self._node = Nodes()
        # self._node.setSceneviewer(self._sceneviewer)
        # self._node.setContext(self._context)

        # Button Groups
        self._ui.colour_group = QtGui.QButtonGroup()
        self._ui.visibility_group = QtGui.QButtonGroup()
        self._ui.visibility_group.setExclusive(False)
        # self._ui.planeGroup.setExclusive(False)

        self._muscles = Musculature()
        self._muscles.setSceneviewer(self._sceneviewer)
        self._muscles.setContext(self._context)
        self._muscles.setListWidget(self._ui.muscle_list)
        self._muscles.setButtonGroups(self._ui.colour_group, self._ui.visibility_group)

        # self._planes = Planes()
        # self._planes.setContext(self._context)

        self.setUpConnections()

        # self._edit_buttons = [
        #     self._ui.create_button,
        #     self._ui.edit_button,
        #     self._ui.deselect_button,
        #     self._ui.delete_button,
        #     self._ui.muscle_rename,
        #     self._ui.delete_button,
        #     self._ui.saveButton,
        #     self._ui.muscle_remove,
        # ]
        #
        # self._edit = False
        #
        # self._no_plane_buttons = [
        #     self._ui.create_button,
        #     self._ui.delete_button,
        #     self._ui.edit_button,
        #     self._ui.deselect_button,
        #     self._ui.planeSilder,
        # ]
        #
        # self._no_plane = False
        # self._position = [50] * 4

    def setUpConnections(self):
        """
        :return:
        """
        self._sceneviewer.graphicsInitialized.connect(self.createGraphics)
        self._ui.viewAllButton.clicked.connect(self._sceneviewer.viewAll)
        # self._ui.saveButton.clicked.connect(self.save)

        ui = self._ui
        # Node Connections
        # node = self._node
        # ui.programMode.buttonClicked.connect(self.setProgramMode)

        # Muscle Connections
        muscle = self._muscles
        ui.muscleGroup.buttonClicked.connect(muscle.processMuscleButton)
        # self._muscles.check_edit_state.connect(self.enableEditing)
        ui.skinButton.toggled.connect(muscle.toggleSkin)
        ui.skeletonButton.toggled.connect(muscle.toggleSkeleton)
        ui.organButton.toggled.connect(muscle.toggleOrgans)
        ui.hideAllMuscles.toggled.connect(self.hideMuscles)

        # Plane
        # self._ui.planeGroup.buttonClicked.connect(self.setPlane)
        # self._ui.planeSilder.sliderMoved.connect(self.movePane)

    # def save(self):
    #     region = self._context.getDefaultRegion()
    #
    #     # Get the save location and name
    #     name, selected_filter = QtGui.QFileDialog.getSaveFileName(self,
    #                                                      caption="Save",
    #                                                      filter="Two EX Files (*.exnode *.exelem);;Single EX File (*.exformat)")
    #
    #     if selected_filter is not None and name is not None:
    #
    #         stream_information = region.createStreaminformationRegion()
    #
    #         if selected_filter == "Two EX Files (*.exnode, *.exelem)":
    #
    #             name = os.path.splitext(os.path.basename(name))[0]
    #             stream_file = stream_information.createStreamresourceFile('{}.exnode'.format(name))
    #             stream_information.setResourceDomainTypes(stream_file, Field.DOMAIN_TYPE_NODES)
    #             stream_file = stream_information.createStreamresourceFile('{}.exelem'.format(name))
    #             stream_information.setResourceDomainTypes(stream_file, Field.DOMAIN_TYPE_MESH1D | Field.DOMAIN_TYPE_MESH2D)
    #         elif selected_filter == "Single EX File (*.exformat)":
    #             stream_information.createStreamresourceFile('{}'.format(name))
    #
    #         status = region.write(stream_information)
    #         if status == OK:
    #             QtGui.QMessageBox.about(self, "Save file status", "Save successful")

    # @QtCore.Slot(QtGui.QAbstractButton)
    # def setPlane(self, button):
    #
    #     name = button.objectName()
    #     position = 3
    #     status = False
    #
    #     # Unchecks all the buttons except the pressed one
    #     # Also saves the location of the current checked button.
    #     for n, button in enumerate(self._ui.planeGroup.buttons()):
    #         if button.objectName() == name:
    #             position = n
    #         else:
    #             if button.isChecked():
    #                 self._position[n] = self._ui.planeSilder.value()
    #             button.setChecked(False)
    #
    #     if name == "planeX":
    #         self._planes.changePlane(0)
    #         status = True
    #     elif name == "planeY":
    #         self._planes.changePlane(1)
    #         status = True
    #     elif name == "planeZ":
    #         self._planes.changePlane(2)
    #         status = True
    #     elif name == "noPlane":
    #         self._planes.changePlane(3)
    #         status = False
    #     else:
    #         raise AttributeError("Button {} doesn't exist".format(name))
    #
    #     self._no_plane = status
    #
    #     for button in self._no_plane_buttons:
    #         button.setEnabled(status)
    #
    #     if not self._edit and status:
    #         for button in self._edit_buttons:
    #             button.setEnabled(False)
    #
    #     self._ui.planeSilder.setValue(self._position[position])

    # @QtCore.Slot(int)
    # def movePane(self, pos):
    #
    #     pos /= float(100)
    #     self._planes.moveCurrentPlane(pos)
    #
    # @QtCore.Slot(int)
    # def moveWheelPane(self, pos):
    #
    #     value = self._ui.planeSilder.value()
    #     pos += value
    #     self._ui.planeSilder.setSliderPosition(pos)
    #     self.movePane(pos)
    #
    # @QtCore.Slot(QtGui.QAbstractButton)
    # def setProgramMode(self, button):
    #
    #     name = button.objectName()
    #
    #     if name == "create_button":
    #         self._sceneviewer.setSelectionMode("CREATE")
    #     elif name == "edit_button":
    #         self._sceneviewer.setSelectionMode("EDIT")
    #     elif name == "view_button":
    #         self._sceneviewer.setSelectionMode("VIEW")

    # @QtCore.Slot()
    # def enableEditing(self, state):
    #
    #     check = self._ui.create_button.isEnabled()
    #     if check == state:
    #         return
    #
    #     self._edit = state
    #     for button in self._edit_buttons:
    #         button.setEnabled(state)
    #
    #     if state:
    #         state = self._no_plane
    #     else:
    #         state = False
    #
    #     for button in self._no_plane_buttons:
    #         button.setEnabled(state)

    @QtCore.Slot()
    def createGraphics(self):
        """
        This function is called when the GL is first initialised.
        :return:
        """
        self.loadSkeleton(self._filenames)
        self.createSkeletonGraphics()
        self.createMuscles()
        self.createOtherGraphics()

        # Create the skin and loads the muscles and organs.

    def loadSkeleton(self, cow_files):
        """
        Loads in the files.
        :return: None
        """

        region = self._context.getDefaultRegion()
        stream_file = region.createStreaminformationRegion()

        for cow_file in cow_files:
            stream_file.createStreamresourceFile(str(cow_file))
        region.read(stream_file)

        # Create a skeleton group and add the whole input into it.
        with get_fieldmodule(region) as fieldmodule:
            skeleton_group = fieldmodule.createFieldGroup()
            one_field = fieldmodule.createFieldConstant(1.0)
            skeleton_group.setManaged(True)
            skeleton_group.setName('skeleton')

            nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
            skeleton_group.createFieldNodeGroup(nodeset).getNodesetGroup().addNodesConditional(one_field)

            mesh_1 = fieldmodule.findMeshByDimension(1)
            skeleton_group.createFieldElementGroup(mesh_1).getMeshGroup().addElementsConditional(one_field)

            mesh_2 = fieldmodule.findMeshByDimension(2)
            skeleton_group.createFieldElementGroup(mesh_2).getMeshGroup().addElementsConditional(one_field)

    def createSkeletonGraphics(self):
        """
        Creates graphics for the skeleton model
        :return: None
        """

        region = self._context.getDefaultRegion()

        with get_materialmodule(region) as material_module:
            bone_material = material_module.findMaterialByName('white')

        with get_fieldmodule(region) as fieldmodule:
            finite_element_field = fieldmodule.findFieldByName('coordinates')
            group = fieldmodule.findFieldByName('skeleton')

        with get_scene(region) as scene:
            surfaces = scene.createGraphicsSurfaces()
            surfaces.setName('skeleton')
            surfaces.setCoordinateField(finite_element_field)
            surfaces.setMaterial(bone_material)
            surfaces.setSelectMode(Graphics.SELECT_MODE_OFF)
            if group.isValid():
                surfaces.setSubgroupField(group)

            # Lower the graphics
            # tessellation = surfaces.getTessellation()
            # tessellation.setMinimumDivisions(1)
            # tessellation.setCircleDivisions(3)
            # tessellation.setRefinementFactors(1)

    def createMuscles(self):

        mypath = 'Pig Files/Muscle'
        filenames = (join(mypath, splitext(f)[0]) for f in listdir(mypath) if isfile(join(mypath, f)))
        filenames = set(filenames)

        if filenames:
            region = self._context.getDefaultRegion().createChild('Muscle')

            for n, filename in enumerate(filenames):
                child_region = region.createChild('{}'.format(split(filename)[1]))
                name = split(filename)[1]

                if child_region.isValid():

                    child_region.beginChange()
                    stream_information = child_region.createStreaminformationRegion()
                    stream_information.createStreamresourceFile('{}.exnode'.format(filename))
                    stream_information.createStreamresourceFile('{}.exelem'.format(filename))
                    child_region.read(stream_information)
                    child_region.endChange()

                    rgb = self._muscles.addMuscleRow(n, 0, name)

                    with get_materialmodule(self._context) as materialmodule:
                        new_material = materialmodule.createMaterial()
                        new_material.setName(name)
                        new_material.setAttributeReal3(Material.ATTRIBUTE_DIFFUSE, rgb)

                    with get_fieldmodule(child_region) as fieldmodule:
                        finite_element_field = fieldmodule.findFieldByName('coordinates')
                        # group = fieldmodule.findFieldByName(name)

                    # if group.isValid():
                    with get_scene(child_region) as scene:
                        surfaces = scene.createGraphicsSurfaces()
                        surfaces.setCoordinateField(finite_element_field)
                        surfaces.setName(name)
                        surfaces.setMaterial(new_material)
                        # surfaces.setSubgroupField(group)



    def createOtherGraphics(self):
        """
        First this loads and creates the graphics for the Organs and then the Skin.
        It creates new regions and then new graphics/materials.
        :return:
        """

        mypath = 'Pig Files/Organ'
        filenames = (join(mypath, splitext(f)[0]) for f in listdir(mypath) if isfile(join(mypath, f)))
        filenames = set(filenames)

        if filenames:
            region = self._context.getDefaultRegion().createChild('Organ')

            with get_materialmodule(region) as materialmodule:
                organ_material = materialmodule.findMaterialByName('tissue')

            for n, filename in enumerate(filenames):

                child_region = region.createChild('Organ_{}'.format(n))
                child_region.beginChange()
                stream_information = child_region.createStreaminformationRegion()
                stream_information.createStreamresourceFile('{}.exnode'.format(filename))
                stream_information.createStreamresourceFile('{}.exelem'.format(filename))
                child_region.read(stream_information)
                child_region.endChange()

                with get_fieldmodule(child_region) as fieldmodule:
                    coordinates = fieldmodule.findFieldByName('coordinates')

                with get_scene(child_region) as scene:
                    surfaces = scene.createGraphicsSurfaces()
                    surfaces.setCoordinateField(coordinates)
                    surfaces.setSelectMode(Graphics.SELECT_MODE_OFF)
                    surfaces.setMaterial(organ_material)

        mypath = 'Pig Files/Skin'
        filenames = (join(mypath, splitext(f)[0]) for f in listdir(mypath) if isfile(join(mypath, f)))
        filenames = set(filenames)

        if filenames:
            region = self._context.getDefaultRegion().createChild('Skin')

            with get_materialmodule(region) as materialmodule:
                organ_material = materialmodule.findMaterialByName('tissue')
                skin_matierial = materialmodule.createMaterial()
                skin_matierial.setAttributeReal(Material.ATTRIBUTE_ALPHA, 0.5)
                skin_matierial.setAttributeReal3(Material.ATTRIBUTE_AMBIENT,
                                                 organ_material.getAttributeReal3(Material.ATTRIBUTE_AMBIENT)[1])
                skin_matierial.setAttributeReal3(Material.ATTRIBUTE_DIFFUSE,
                                                 organ_material.getAttributeReal3(Material.ATTRIBUTE_DIFFUSE)[1])
                skin_matierial.setAttributeReal3(Material.ATTRIBUTE_EMISSION,
                                                 organ_material.getAttributeReal3(Material.ATTRIBUTE_EMISSION)[1])
                skin_matierial.setAttributeReal(Material.ATTRIBUTE_SHININESS,
                                                 organ_material.getAttributeReal(Material.ATTRIBUTE_SHININESS))
                skin_matierial.setAttributeReal3(Material.ATTRIBUTE_SPECULAR,
                                                 organ_material.getAttributeReal3(Material.ATTRIBUTE_SPECULAR)[1])

            for n, filename in enumerate(filenames):
                child_region = region.createChild('Skin_{}'.format(n))
                child_region.beginChange()
                stream_information = child_region.createStreaminformationRegion()
                stream_information.createStreamresourceFile('{}.exnode'.format(filename))
                stream_information.createStreamresourceFile('{}.exelem'.format(filename))
                child_region.read(stream_information)
                child_region.endChange()

                with get_fieldmodule(child_region) as fieldmodule:
                    coordinates = fieldmodule.findFieldByName('coordinates')

                with get_scene(child_region) as scene:
                    surfaces = scene.createGraphicsSurfaces()
                    surfaces.setCoordinateField(coordinates)
                    surfaces.setSelectMode(Graphics.SELECT_MODE_OFF)
                    surfaces.setMaterial(skin_matierial)

    def setupTool(self):
        region = self._context.getDefaultRegion()

        with get_fieldmodule(region) as fieldmodule:
            finite_element_field = fieldmodule.findFieldByName('coordinates')
            # Sets the node create coordinate field

        # self._tool = ZincInteractiveTool()
        self._sceneviewer.setTool(self._tool)
        self._node.setTool(self._tool)

        # Connections
        # self._ui.delete_button.clicked.connect(self._tool.deleteSelectedNodesAndElements)
        self._muscles.muscle_name.connect(self._tool.setMuscleGroup)
        # self._ui.deselect_button.clicked.connect(self._tool.deselect)
        self._sceneviewer.mouseMoved.connect(self.moveWheelPane)

    @QtCore.Slot(bool)
    def hideMuscles(self, state):
        self._muscles.hideAllMuscles(state)
        self._ui.muscle_list.setEnabled(not state)
