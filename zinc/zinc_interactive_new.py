#!/usr/bin/python

try:
    from PySide import QtGui, QtCore, QtGui
except ImportError:
    from PyQt4 import QtCore, QtGui

import math

from opencmiss.zinc.element import Element, Elementbasis
from opencmiss.zinc.field import Field, FieldGroup, FieldFindMeshLocation
from opencmiss.zinc.glyph import Glyph
from opencmiss.zinc.graphics import Graphics
from opencmiss.zinc.scenecoordinatesystem import SCENECOORDINATESYSTEM_LOCAL, \
    SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT, SCENECOORDINATESYSTEM_WORLD
from opencmiss.zinc.sceneviewerinput import Sceneviewerinput
from opencmiss.zinc.status import *

from zinc_helper import *

# mapping from qt to zinc start
# Create a button map of Qt mouse buttons to Zinc input buttons
button_map = {QtCore.Qt.LeftButton: Sceneviewerinput.BUTTON_TYPE_LEFT,
              QtCore.Qt.MidButton: Sceneviewerinput.BUTTON_TYPE_MIDDLE,
              QtCore.Qt.RightButton: Sceneviewerinput.BUTTON_TYPE_RIGHT}


# Create a modifier map of Qt modifier keys to Zinc modifier keys
def modifier_map(qt_modifiers):
    """
    Return a Zinc SceneViewerInput modifiers object that is created from
    the Qt modifier flags passed in.
    """
    modifiers = Sceneviewerinput.MODIFIER_FLAG_NONE
    if qt_modifiers & QtCore.Qt.SHIFT:
        modifiers = modifiers | Sceneviewerinput.MODIFIER_FLAG_SHIFT

    return modifiers

SELECTION_RUBBERBAND_NAME = 'selection_rubberband'


class NodeEditInfo(object):

    def __init__(self):
        self.reset()

    def reset(self):
        self._node = None
        self._graphics = None
        self._coordinateField = None
        self._orientationField = None
        self._glyphCentre = [0.0, 0.0, 0.0]
        self._glyphSize = [0.0, 0.0, 0.0]
        self._glyphScaleFactors = [0.0, 0.0, 0.0]
        self._variableScaleField = Field()
        self._nearestElement = None
        self._elementCoordinateField = None
        self._createCoordinatesField = None


class ZincInteractiveTool(QtCore.QObject):

    QtCore.pyqtSignal = QtCore.Signal
    QtCore.pyqtSlot = QtCore.Slot

    def __init__(self):
        super(ZincInteractiveTool, self).__init__()
        self._nodeEditInfo = NodeEditInfo()

        self._coordinateField = None
        self._nodeTemplate = None
        self._nodeConstrainMode = False

        self._scene = None
        self._sceneviewer = None
        self._scenepicker = None

        self._selectionGroup = None
        self._selectionBox = None
        self._fieldGroup = None
        self._selectedElement = None

        self._nodeList = []
        self._pos_start = None
        self._move_start = None

        self._graphics = None

        self._window_coords_from = None
        self._global_coords_from = None
        self._global_coords_to = None
        self._window_coords_to = None

    def setupInteraction(self, sceneviewer, scenepicker, coordinateField):

        self._sceneviewer = sceneviewer
        self._scenepicker = scenepicker
        self._scene = self._scenepicker.getScene()
        self._selectionGroup = self._scene.getSelectionField().castGroup()
        if coordinateField.isValid():
            self._coordinateField = coordinateField

            with get_fieldmodule(coordinateField) as fieldmodule:
                nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                if nodeset.isValid():
                    self._nodeTemplate = nodeset.createNodetemplate()
                    self._nodeTemplate.defineField(coordinateField)

                    # Set up unproject pipeline
                    self._window_coords_from = fieldmodule.createFieldConstant([0, 0, 0])
                    self._global_coords_from = fieldmodule.createFieldConstant([0, 0, 0])
                    unproject = fieldmodule.createFieldSceneviewerProjection(self._sceneviewer,
                                                                             SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT,
                                                                             SCENECOORDINATESYSTEM_WORLD)
                    project = fieldmodule.createFieldSceneviewerProjection(self._sceneviewer,
                                                                           SCENECOORDINATESYSTEM_WORLD,
                                                                           SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT)

                    self._global_coords_to = fieldmodule.createFieldProjection(self._window_coords_from, unproject)
                    self._window_coords_to = fieldmodule.createFieldProjection(self._global_coords_from, project)

            with get_scene(sceneviewer) as scene:
                self._selectionBox = scene.createGraphicsPoints()
                self._selectionBox.setName(SELECTION_RUBBERBAND_NAME)
                self._selectionBox.setScenecoordinatesystem(SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT)

                attributes = self._selectionBox.getGraphicspointattributes()
                attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_CUBE_WIREFRAME)
                attributes.setBaseSize([10, 10, 0.9999])
                attributes.setGlyphOffset([1, -1, 0])
                self._selectionBox.setVisibilityFlag(False)

    @QtCore.Slot(FieldGroup)
    def setMuscleGroup(self, group):
        self._fieldGroup = group

    def deleteSelectedNodesAndElements(self):
        if self._selectionGroup.isValid() and not self._selectionGroup.isEmpty():

            # Go through the elements and remove anything in the selection group
            with get_fieldmodule(self._selectionGroup) as fieldmodule:
                mesh_2 = fieldmodule.findMeshByDimension(2)
                mesh_1 = fieldmodule.findMeshByDimension(1)
                nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)

                mesh_2.destroyElementsConditional(self._selectionGroup)
                mesh_1.destroyElementsConditional(self._selectionGroup)
                nodeset.destroyNodesConditional(self._selectionGroup)

                self._selectionGroup.clearLocal()

    def createNode(self, pos):
        node = self.nodeIsSelectedAtCoordinates(*pos)

        # No Node then create it
        if not node.isValid():

            # TODO add element on click.
            with get_scene(self._scenepicker):
                self._graphics.setSelectMode(Graphics.SELECT_MODE_ON)
                self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                      pos[0] - 1, pos[1] - 1, pos[0] + 1, pos[1] + 1)
                element = self._scenepicker.getNearestElement()
                self._graphics.setSelectMode(Graphics.SELECT_MODE_OFF)

            if element.isValid():
                status, coordinates = self._scenepicker.getPickingVolumeCentre()
                if status == OK:
                    coordinates = self.getNearestSurfaceCoordinate(pos, element)
                    self.createNodeAtCoordinates(coordinates)
                    return
            else:
                status, coordinates = self._scenepicker.getPickingVolumeCentre()

            if status == OK:
                self.createNodeAtCoordinates(coordinates)

    def createElement(self, pos):
        """
        Checks if there is a node at the selected position. If so then it adds it to the current selection.
        If the NodeGroupField is not valid then the helper is used to create it.
        Checks for the size of the selection group.
        """
        node = self.nodeIsSelectedAtCoordinates(*pos)

        if node.isValid():

            with get_fieldmodule(self._selectionGroup) as fieldmodule:
                nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                nodeGroupField = self._selectionGroup.getFieldNodeGroup(nodeset)
                if nodeGroupField.isValid():
                    nodesetGroup = nodeGroupField.getNodesetGroup()
                    node_exists = nodesetGroup.containsNode(node)

                    if node_exists:
                        nodesetGroup.removeNode(node)
                        self._nodeList.remove(node.getIdentifier())

                    else:
                        size = nodesetGroup.getSize()
                        if size == 3:
                            self._scenepicker.addPickedNodesToFieldGroup(self._selectionGroup)
                            self._nodeList.append(node.getIdentifier())
                            self.create2DFiniteElementfromSelection()
                            self._nodeList = []
                            self._selectionGroup.clear()
                        else:
                            if size == 0:
                                self._nodeList = []
                            self._nodeList.append(node.getIdentifier())
                            self._scenepicker.addPickedNodesToFieldGroup(self._selectionGroup)
                else:
                    self._nodeList.append(node.getIdentifier())
                    self._scenepicker.addPickedNodesToFieldGroup(self._selectionGroup)

    def moveNodes(self, pos):
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                  pos[0] - 1, pos[1] - 1,
                                                  pos[0] + 1, pos[1] + 1)

        element = self._selectedElement

        if element.isValid():
            coordinates = self.getNearestSurfaceCoordinate(pos, element)
            projectCoordinates = self.project(coordinates[0], coordinates[1], coordinates[2])
            coordinates = self.unproject(pos[0], pos[1] * -1.0, projectCoordinates[2])

            diff = coordinates

            with get_fieldmodule(self._selectionGroup) as fieldmodule:
                fieldcache = fieldmodule.createFieldcache()
                if self._selectionGroup.isValid():
                    nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                    nodeGroup = self._selectionGroup.getFieldNodeGroup(nodeset)
                    if nodeGroup.isValid():
                        nodesetGroup = nodeGroup.getNodesetGroup()
                        size = nodesetGroup.getSize()
                        nodeIterator = nodesetGroup.createNodeiterator()

                        for n in range(size):
                            node = nodeIterator.next()
                            fieldcache.setNode(node)
                            status, node_coordinates = self._coordinateField.evaluateReal(fieldcache, 3)

                            if n == 0:
                                self._coordinateField.assignReal(fieldcache, coordinates)
                                diff = [coordinates[i] - node_coordinates[i] for i in range(3)]
                            else:
                                self._coordinateField.assignReal(
                                    fieldcache,
                                    [sum(x) for x in zip(node_coordinates, diff)]
                                )

    def selectGraphic(self, pos):

        flag = self._selectionBox.getVisibilityFlag()
        if flag:

            x = sorted([pos[0], self._pos_start[0]])
            y = sorted([pos[1], self._pos_start[1]])

            with get_scene(self._selectionBox) as scene:
                self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                    x[0], y[0], x[1], y[1])
                self._scenepicker.addPickedNodesToFieldGroup(self._selectionGroup)
                self._scenepicker.addPickedElementsToFieldGroup(self._selectionGroup)
                self._selectionBox.setVisibilityFlag(False)
        else:
            self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                      pos[0] - 1, pos[1] - 1, pos[0] + 1, pos[1] + 1)
            node = self._scenepicker.getNearestNode()
            if node.isValid():

                with get_fieldmodule(self._selectionGroup) as fieldmodule:
                    nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                    nodeGroupField = self._selectionGroup.getFieldNodeGroup(nodeset)
                    if nodeGroupField.isValid():
                        nodesetGroup = nodeGroupField.getNodesetGroup()
                        node_exists = nodesetGroup.containsNode(node)

                        if node_exists:
                            nodesetGroup.removeNode(node)

                        else:
                            self._scenepicker.addPickedNodesToFieldGroup(self._selectionGroup)
                    else:
                        self._scenepicker.addPickedNodesToFieldGroup(self._selectionGroup)
                return

            element = self._scenepicker.getNearestElement()
            if element.isValid():

                with get_fieldmodule(self._selectionGroup) as fieldmodule:
                    mesh = fieldmodule.findMeshByDimension(2)
                    ElementGroupField = self._selectionGroup.getFieldElementGroup(mesh)
                    if ElementGroupField .isValid():
                        meshGroup = ElementGroupField .getMeshGroup()
                        element_exists = meshGroup.containsElement(element)

                        if element_exists:
                            meshGroup.removeElement(element)
                        else:
                            self._scenepicker.addPickedElementsToFieldGroup(self._selectionGroup)
                    else:
                        self._scenepicker.addPickedElementsToFieldGroup(self._selectionGroup)
                return

    def drawBox(self, pos):

        diff = [float(pos[i] - self._pos_start[i]) for i in range(2)]
        for count, num in enumerate(diff):
            if abs(num) < 0.0001:
                diff[count] = 1.0
        off = [float(self._pos_start[i] / diff[i]) + 0.5 for i in range(2)]

        # update the selection box with a new offset and new size.
        with get_scene(self._scenepicker) as scene:
            attributes = self._selectionBox.getGraphicspointattributes()
            attributes.setBaseSize([diff[0], diff[1], 0.9999])
            attributes.setGlyphOffset([off[0], -off[1], 0])

        self._selectionBox.setVisibilityFlag(True)

    def setMoveStart(self, pos):
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                  pos[0] - 1, pos[1] - 1, pos[0] + 1, pos[1] + 1)
        status, temp = self._scenepicker.getPickingVolumeCentre()
        if status:
            self._move_start = temp
            self._pos_start = pos
        else:
            "Error"

        with get_scene(self._scenepicker) as scene:
            self._graphics.setSelectMode(Graphics.SELECT_MODE_ON)
            element = self._scenepicker.getNearestElement()
            self._graphics.setSelectMode(Graphics.SELECT_MODE_OFF)

        if element.isValid():
            self._selectedElement = element

    def getNearestSurfacesElementAndCoordinates(self, x, y):
        """
        Return element and its coordinates field if its valid
        """
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                  x - 3, y - 3, x + 3, y + 3)
        nearestGraphics = self._scenepicker.getNearestGraphics()
        if nearestGraphics.isValid():
            surfacesGraphics = nearestGraphics.castSurfaces()
            if surfacesGraphics.isValid():
                nearestElement = self._scenepicker.getNearestElement()
                return True, nearestElement
        return False, None

    def nodeIsSelectedAtCoordinates(self, x, y):
        """
        Return node and its coordinates field if its valid
        """
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                  x - 3, y - 3,
                                                  x + 3, y + 3)
        return self._scenepicker.getNearestNode()

    def createNodeAtCoordinates(self, newCoordinates):
        """
        Create a new node at a location based on information provided by nodeEditInfo
        Also adds a delete to the Undo List
        """

        with get_fieldmodule(self._coordinateField) as fieldmodule:

            nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
            node = nodeset.createNode(-1, self._nodeTemplate)
            fieldcache = fieldmodule.createFieldcache()
            fieldcache.setNode(node)
            self._coordinateField.assignReal(fieldcache, newCoordinates)

    def create2DFiniteElementfromSelection(self):
        """
        Creates a 2D finite element from selection moves the nodes into a subgroup with a certain name.
        """
        fieldmodule = self._coordinateField.getFieldmodule()
        finite_element_field = self._coordinateField

        nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
        mesh = fieldmodule.findMeshByDimension(2)

        group = self._fieldGroup
        nodesetgroup = group.getFieldNodeGroup(nodeset).getNodesetGroup()
        meshgroup = group.getFieldElementGroup(mesh).getMeshGroup()

        element_template = meshgroup.createElementtemplate()
        element_template.setElementShapeType(Element.SHAPE_TYPE_SQUARE)
        element_node_count = 4
        element_template.setNumberOfNodes(element_node_count)

        linear_basis = fieldmodule.createElementbasis(2, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
        node_indexes = [1, 2, 3, 4]
        element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

        for i, node_identifier in enumerate(self._nodeList):
            node = nodeset.findNodeByIdentifier(node_identifier)
            if nodesetgroup.isValid():
                nodesetgroup.addNode(node)
            element_template.setNode(i + 1, node)

        if meshgroup.isValid():
            meshgroup.addElement(meshgroup.createElement(-1, element_template))
        else:
            mesh.defineElement(-1, element_template)

    def getNearestSurfaceCoordinate(self, pos, element):

        with get_fieldmodule(self._coordinateField) as fieldmodule:

            converged = False
            fieldcache = fieldmodule.createFieldcache()
            fieldcache.setMeshLocation(element, [0.5] * 3)
            temp, unprojectCoordinates = self._scenepicker.getPickingVolumeCentre()
            if temp == OK:
                fieldcache.clearLocation()
                mesh = element.getMesh()
                meshGroup = fieldmodule.createFieldGroup().createFieldElementGroup(mesh).getMeshGroup()
                meshGroup.addElement(element)
                return_code = True
                steps = 0
                point = unprojectCoordinates

                while return_code and not converged:
                    previous_point = point
                    fieldLocation = fieldmodule.createFieldFindMeshLocation(self._coordinateField,
                                                                            self._coordinateField, meshGroup)
                    fieldLocation.setSearchMode(FieldFindMeshLocation.SEARCH_MODE_NEAREST)
                    fieldcache.setFieldReal(self._coordinateField, previous_point)
                    element, chartCoordinates = fieldLocation.evaluateMeshLocation(fieldcache, 3)
                    fieldcache.setMeshLocation(element, chartCoordinates)
                    return_code, newCoordinates = self._coordinateField.evaluateReal(fieldcache, 3)

                    changes = [point[0] - newCoordinates[0], point[1] - newCoordinates[1],
                               point[2] - newCoordinates[2]]
                    if math.sqrt(changes[0] * changes[0] + changes[1] * changes[1] + changes[2] * changes[2]) < 1.0e-4:
                        converged = True
                    else:
                        point[0] = newCoordinates[0]
                        point[1] = newCoordinates[1]
                        point[2] = newCoordinates[2]
                        steps += 1
                        if steps > 1000:
                            return_code = False
                        projectCoordinates = self.project(*point)
                        point = self.unproject(pos[0] * 1.0, pos[1] * -1.0, projectCoordinates[2])
                        changes = [point[0] - previous_point[0], point[1] - previous_point[1],
                                   point[2] - previous_point[2]]
                        if math.sqrt(changes[0] * changes[0] + changes[1] * changes[1] + changes[2] * changes[2]) < 1.0e-6:
                            return_code = False

                return point

    def project(self, x, y, z):
        """
        Project the given point in global coordinates into window coordinates
        with the origin at the window's top left pixel.
        """
        in_coords = [x, y, z]
        with get_fieldmodule(self._coordinateField) as fieldmodule:
            fieldcache = fieldmodule.createFieldcache()
            self._global_coords_from.assignReal(fieldcache, in_coords)
            result, out_coords = self._window_coords_to.evaluateReal(fieldcache, 3)
        if result == OK:
            return out_coords

        return None

    def unproject(self, x, y, z):
        """
        Unproject the given point in window coordinates where the origin is
        at the window's top left pixel into global coordinates.  The z value
        is a depth which is mapped so that 0 is on the near plane and 1 is
        on the far plane.
        ???GRC -1 on the far and +1 on the near clipping plane
        """
        in_coords = [x, y, z]
        with get_fieldmodule(self._coordinateField) as fieldmodule:
            fieldcache = fieldmodule.createFieldcache()
            self._window_coords_from.assignReal(fieldcache, in_coords)
            result, out_coords = self._global_coords_to.evaluateReal(fieldcache, 3)
        if result == OK:
            return out_coords

        return None

    def deselect(self):
        self._selectionGroup.clearLocal()

    def setPlane(self, graphic):
        self._graphics = graphic
