import math

from opencmiss.zinc.element import Element, Elementbasis
from opencmiss.zinc.field import Field, FieldFindMeshLocation
from opencmiss.zinc.glyph import Glyph


def createCoordinatesVectorsGraphics(scene, coordinateField, valueLabel,
                                             versionNumber, baseSizes, scaleFactors, selectMode,
                                             material, selectedMaterial):
    """
    Create graphics for the vector of the supplied scene which allow the vector to be edited.
    """
    fieldmodule = scene.getRegion().getFieldmodule()
    derivativeField = fieldmodule.createFieldNodeValue(coordinateField, valueLabel, versionNumber)
    if derivativeField.isValid():
        nodes = scene.createGraphicsPoints()
        nodes.setCoordinateField(coordinateField)
        nodes.setFieldDomainType(Field.DOMAIN_TYPE_NODES)
        nodes.setSelectMode(selectMode)
        nodes.setMaterial(material)
        nodes.setSelectedMaterial(selectedMaterial)
        attributes = nodes.getGraphicspointattributes()
        attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_ARROW_SOLID)
        attributes.setBaseSize(baseSizes)
        attributes.setScaleFactors(scaleFactors)
        attributes.setOrientationScaleField(derivativeField)
        # gfx modify g_element bicubic_linear node_points coordinate coordinates glyph arrow_solid size "0*0.1*0.1"
        # scale_factors "0.5*0*0" orientation dx_ds1 mat gold selected_mat gold draw_selected
    return None


def makeGlyphOrientationScaleAxes(orientationScaleValues):
    num = len(orientationScaleValues)
    size = [0.0, 0.0, 0.0]
    axis1 = [0.0, 0.0, 0.0]
    axis2 = [0.0, 0.0, 0.0]
    axis3 = [0.0, 0.0, 0.0]
    if num == 0:
        size = [0.0, 0.0, 0.0]
        axis1 = [1.0, 0.0, 0.0]
        axis2 = [0.0, 1.0, 0.0]
        axis3 = [0.0, 0.0, 1.0]
    elif num == 1:
        size = [orientationScaleValues[0], orientationScaleValues[1], orientationScaleValues[2]]
        axis1 = [1.0, 0.0, 0.0]
        axis2 = [0.0, 1.0, 0.0]
        axis3 = [0.0, 0.0, 1.0]
    elif num == 2:
        axis1 = [orientationScaleValues[0], orientationScaleValues[1], 0.0]
        magnitude = math.sqrt(axis1[0]*axis1[0]+axis1[1]*axis1[1])
        if magnitude > 0.0:
            axis1[0] /= magnitude
            axis1[1] /= magnitude
        size = [magnitude, magnitude, magnitude]
        axis2 = [-axis1[1], axis1[0], 0.0]
        axis3 = [0.0, 0.0, 1.0]
    elif num == 3:
        axis1 = orientationScaleValues
        magnitude = math.sqrt(axis1[0]*axis1[0]+axis1[1]*axis1[1]+axis1[2]*axis1[2])
        if magnitude > 0.0:
            axis1[0] /= magnitude
            axis1[1] /= magnitude
            axis1[2] /= magnitude
            size = [magnitude, magnitude, magnitude]
            axis3 = [0.0, 0.0, 0.0]
            if math.fabs(axis1[0]) < math.fabs(axis1[1]):
                if math.fabs(axis1[2]) < math.fabs(axis1[0]):
                    axis3[2] = 1.0
                else:
                    axis3[0] = 1.0
            else:
                if math.fabs(axis1[2]) < math.fabs(axis1[1]):
                    axis3[2] = 1.0
                else:
                    axis3[1] = 1.0
            axis2[0] = axis3[1]*axis1[2]-axis3[2]*axis1[1]
            axis2[1] = axis3[2]*axis1[0]-axis3[0]*axis1[2]
            axis2[2] = axis3[0]*axis1[1]-axis3[1]*axis1[0]
            magnitude = math.sqrt(axis2[0]*axis2[0]+axis2[1]*axis2[1]+axis2[2]*axis2[2])
            axis2[0] /= magnitude
            axis2[1] /= magnitude
            axis2[2] /= magnitude
            axis3[0] = axis1[1]*axis2[2]-axis1[2]*axis2[1]
            axis3[1] = axis1[2]*axis2[0]-axis1[0]*axis2[2]
            axis3[2] = axis1[0]*axis2[1]-axis1[1]*axis2[0]
        else:
            axis2 =[0.0, 0.0, 0.0]
            axis3 = [0.0, 0.0, 0.0]
            size = [0.0, 0.0, 0.0]
    elif num == 4 or num == 6:
        if num == 4:
            axis1 = [orientationScaleValues[0], orientationScaleValues[1], 0.0]
            axis2 = [orientationScaleValues[2], orientationScaleValues[3], 0.0]
        else:
            axis1 = [orientationScaleValues[0], orientationScaleValues[1], orientationScaleValues[2]]
            axis2 = [orientationScaleValues[3], orientationScaleValues[4], orientationScaleValues[5]]
        axis3[0] = axis1[1]*axis2[2]-axis1[2]*axis2[1]
        axis3[1] = axis1[2]*axis2[0]-axis1[0]*axis2[2]
        axis3[2] = axis1[0]*axis2[1]-axis1[1]*axis2[0]
        magnitude=math.sqrt(axis1[0]*axis1[0]+axis1[1]*axis1[1]+axis1[2]*axis1[2])
        if magnitude > 0.0:
            axis1[0] /= magnitude
            axis1[1] /= magnitude
            axis1[2] /= magnitude
        size[0] = magnitude
        magnitude=math.sqrt(axis2[0]*axis2[0]+axis2[1]*axis2[1]+axis2[2]*axis2[2])
        if magnitude > 0.0:
            axis2[0] /= magnitude
            axis2[1] /= magnitude
            axis2[2] /= magnitude
        size[1] = magnitude
        magnitude = math.sqrt(axis3[0]*axis3[0]+axis3[1]*axis3[1]+axis3[2]*axis3[2])
        if magnitude > 0.0:
            axis3[0] /= magnitude
            axis3[1] /= magnitude
            axis3[2] /= magnitude
        size[2] = magnitude
    elif num == 9:
        axis1 = [orientationScaleValues[0], orientationScaleValues[1], orientationScaleValues[2]]
        magnitude = math.sqrt(axis1[0]*axis1[0]+axis1[1]*axis1[1]+axis1[2]*axis1[2])
        if magnitude > 0.0:
            axis1[0] /= magnitude
            axis1[1] /= magnitude
            axis1[2] /= magnitude
        size[0] = magnitude
        axis2 = [orientationScaleValues[3], orientationScaleValues[4], orientationScaleValues[5]]
        magnitude = math.sqrt(axis2[0]*axis2[0]+axis2[1]*axis2[1]+axis2[2]*axis2[2])
        if magnitude > 0.0:
            axis2[0] /= magnitude
            axis2[1] /= magnitude
            axis2[2] /= magnitude
        size[1] = magnitude
        axis3 = [orientationScaleValues[6], orientationScaleValues[7], orientationScaleValues[8]]
        magnitude = math.sqrt(axis3[0]*axis3[0]+axis3[1]*axis3[1]+axis3[2]*axis3[2])
        if magnitude > 0.0:
            axis3[0] /= magnitude
            axis3[1] /= magnitude
            axis3[2] /= magnitude
        size[2] = magnitude
    return axis1, axis2, axis3, size


def elementConstrainFunction(fieldmodule, fieldcache, coordinates, elementCoordinateField, meshGroup):
    """
    Return new coordinates which is constrained to the meshGroup
    """
    fieldLocation = fieldmodule.createFieldFindMeshLocation(elementCoordinateField,
                                                            elementCoordinateField, meshGroup)
    fieldLocation.setSearchMode(FieldFindMeshLocation.SEARCH_MODE_NEAREST)
    fieldcache.setFieldReal(elementCoordinateField, coordinates)
    element, chartCoordinates = fieldLocation.evaluateMeshLocation(fieldcache, 3)
    fieldcache.setMeshLocation(element, chartCoordinates)
    return_code, newCoordinates = elementCoordinateField.evaluateReal(fieldcache, 3)
    return True, newCoordinates


def updateNodeVectorWithDelta(fieldcache, orientationField, node, xdiff, ydiff, zdiff):
    """
    Updated the orientation field of a node with delta
    """
    fieldcache.setNode(node)
    numberOfComponents = orientationField.getNumberOfComponents()
    return_code, orientationScale = orientationField.evaluateReal(fieldcache, numberOfComponents)
    if numberOfComponents == 1:
        orientationScale[0] = xdiff
    elif numberOfComponents == 2 or numberOfComponents == 4:
        orientationScale[0] = xdiff
        orientationScale[1] = ydiff
    elif numberOfComponents == 3 or numberOfComponents == 6 or numberOfComponents == 9:
        orientationScale[0] = xdiff
        orientationScale[1] = ydiff
        orientationScale[2] = zdiff
    orientationField.assignReal(fieldcache, orientationScale)


def create1DElement(fieldmodule, finite_element_field, node_coordinates, group=None):
    """
    Create a 2D finite element from the fieldmodule, finite_element_field and node_coordinates.
    """

    nodeset = fieldmodule.findNodesetByName('nodes')
    mesh = fieldmodule.findMeshByDimension(1)
    nodeset_group = None
    mesh_group = None

    if group is not None:
        group = fieldmodule.findFieldByName(group).castGroup()

        nodeset_group = group.getFieldNodeGroup(nodeset).getNodesetGroup()
        if not nodeset_group.isValid():
            nodeset_group = group.createFieldNodeGroup(nodeset)
            nodeset_group.setManaged(True)
            nodeset_group = nodeset_group.getNodesetGroup()
        mesh_group = group.getFieldElementGroup(mesh).getMeshGroup()
        if not mesh_group.isValid():
            mesh_group = group.createFieldElementGroup(mesh)
            mesh_group.setManaged(True)
            mesh_group = mesh_group.getMeshGroup()

    node_template = nodeset.createNodetemplate()
    node_template.defineField(finite_element_field)

    field_cache = fieldmodule.createFieldcache()

    node_identifiers = []
    for node_coordinate in node_coordinates:
        node = nodeset.createNode(-1, node_template)
        node_identifiers.append(node.getIdentifier())
        field_cache.setNode(node)
        finite_element_field.assignReal(field_cache, node_coordinate)
        if group:
            nodeset_group.addNode(node)

    element_template = mesh.createElementtemplate()
    element_template.setElementShapeType(Element.SHAPE_TYPE_LINE)
    element_node_count = 2
    element_template.setNumberOfNodes(element_node_count)
    linear_basis = fieldmodule.createElementbasis(1, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
    node_indexes = [1, 2]
    element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

    for i, node_identifier in enumerate(node_identifiers):
        node = nodeset.findNodeByIdentifier(node_identifier)
        element_template.setNode(i+1, node)

    if group is not None:
        element = mesh.createElement(-1, element_template)
        mesh_group.addElement(element)
    else:
        mesh.defineElement(-1, element_template)


def create2DFiniteElement(fieldmodule, finite_element_field, node_coordinates, group=None):
    """
    Create a 2D finite element from the fieldmodule, finite_element_field and node_coordinates.
    :param fieldmodule: The field module
    :param finite_element_field: The coordinate field to add the nodes into and set their location
    :param node_coordinates: The four sets of coordinates to create nodes at
    :param group: The group to
    """

    nodeset = fieldmodule.findNodesetByName('nodes')
    mesh = fieldmodule.findMeshByDimension(2)
    nodeset_group = None
    mesh_group = None

    if group is not None:
        group = fieldmodule.findFieldByName(group).castGroup()

        nodeset_group = group.getFieldNodeGroup(nodeset).getNodesetGroup()
        if not nodeset_group.isValid():
            nodeset_group = group.createFieldNodeGroup(nodeset)
            nodeset_group.setManaged(True)
            nodeset_group = nodeset_group.getNodesetGroup()
        mesh_group = group.getFieldElementGroup(mesh).getMeshGroup()
        if not mesh_group.isValid():
            mesh_group = group.createFieldElementGroup(mesh)
            mesh_group.setManaged(True)
            mesh_group = mesh_group.getMeshGroup()

    node_template = nodeset.createNodetemplate()
    node_template.defineField(finite_element_field)

    field_cache = fieldmodule.createFieldcache()

    node_identifiers = []
    for node_coordinate in node_coordinates:
        node = nodeset.createNode(-1, node_template)
        node_identifiers.append(node.getIdentifier())
        field_cache.setNode(node)
        finite_element_field.assignReal(field_cache, node_coordinate)
        if group is not None:
            nodeset_group.addNode(node)

    element_template = mesh.createElementtemplate()
    element_template.setElementShapeType(Element.SHAPE_TYPE_SQUARE)
    element_node_count = 4
    element_template.setNumberOfNodes(element_node_count)
    linear_basis = fieldmodule.createElementbasis(2, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
    node_indexes = [1, 2, 3, 4]
    element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

    for i, node_identifier in enumerate(node_identifiers):
        node = nodeset.findNodeByIdentifier(node_identifier)
        element_template.setNode(i+1, node)

    if group is not None:
        element = mesh.createElement(-1, element_template)
        mesh_group.addElement(element)
    else:
        mesh.defineElement(-1, element_template)


def updateNodePositionWithDelta(fieldcache, coordinateField, node, xdiff, ydiff, zdiff):
    """
    Updated coordinates of a single node with delta
    """
    fieldcache.setNode(node)
    return_code, coordinates = coordinateField.evaluateReal(fieldcache, 3)
    coordinateField.assignReal(fieldcache, [coordinates[0]+xdiff,
                                            coordinates[1]+ydiff,
                                            coordinates[2]+zdiff])


def create3DFiniteElement(fieldmodule, finite_element_field, node_coordinates, group=None):
    """
    Create a 2D finite element from the fieldmodule, finite_element_field and node_coordinates.
    """

    nodeset = fieldmodule.findNodesetByName('nodes')
    mesh = fieldmodule.findMeshByDimension(3)
    nodeset_group = None
    mesh_group = None

    if group is not None:
        group = fieldmodule.findFieldByName(group).castGroup()

        nodeset_group = group.getFieldNodeGroup(nodeset).getNodesetGroup()
        if not nodeset_group.isValid():
            nodeset_group = group.createFieldNodeGroup(nodeset)
            nodeset_group.setManaged(True)
            nodeset_group = nodeset_group.getNodesetGroup()
        mesh_group = group.getFieldElementGroup(mesh).getMeshGroup()
        if not mesh_group.isValid():
            mesh_group = group.createFieldElementGroup(mesh)
            mesh_group.setManaged(True)
            mesh_group = mesh_group.getMeshGroup()

    node_template = nodeset.createNodetemplate()
    node_template.defineField(finite_element_field)

    field_cache = fieldmodule.createFieldcache()

    node_identifiers = []
    for node_coordinate in node_coordinates:
        node = nodeset.createNode(-1, node_template)
        node_identifiers.append(node.getIdentifier())
        field_cache.setNode(node)
        finite_element_field.assignReal(field_cache, node_coordinate)
        if group is not None:
            nodeset_group.addNode(node)

    element_template = mesh.createElementtemplate()
    element_template.setElementShapeType(Element.SHAPE_TYPE_CUBE)
    element_node_count = 8
    element_template.setNumberOfNodes(element_node_count)
    linear_basis = fieldmodule.createElementbasis(3, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
    node_indexes = [1, 2, 3, 4, 5, 6, 7, 8]
    element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

    for i, node_identifier in enumerate(node_identifiers):
        node = nodeset.findNodeByIdentifier(node_identifier)
        element_template.setNode(i+1, node)

    if group is not None:
        element = mesh.createElement(-1, element_template)
        mesh_group.addElement(element)
    else:
        mesh.defineElement(-1, element_template)
